import { Component, OnInit, Input } from '@angular/core';
import { Pais } from '../app.component';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Input() country:Pais;
  constructor() { }

  ngOnInit() {
    console.log(this.country)
  }

}
