import { Component } from '@angular/core';
import axios  from 'axios'  ;

export interface Pais{
  name:string;
  capital:string;
  region:string;
  population:number;
  languages:[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend-biblioteca';
  countries:Pais[];
  country:Pais;
  ngOnInit(){
    axios.get('https://restcountries.eu/rest/v2/all')
    .then(response =>{
      this.countries= response.data as Pais[]
      console.log(this.countries)
    })
  }

  getCountry(country:Pais){
    console.log(country);
    this.country=country;
  }
}
